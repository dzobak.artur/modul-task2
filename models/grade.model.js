const mongoose = require('mongoose');

const requestSchema = new mongoose.Schema({
  timestamp: {
    type: Date,
    default: Date.now,
  },
  inputData: {
    type: Object,
    required: true,
  },
  result: {
    type: Number,
    required: true,
  },
});

module.exports = mongoose.model('Request', requestSchema);
