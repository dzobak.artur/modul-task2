const Triangle = require('../models/grade.model');
async function saveRequest(inputData, result) {
    const request = new Triangle({
      inputData,
      result,
    });
    return await request.save();
  }
  
  module.exports = {
    saveRequest,
  };