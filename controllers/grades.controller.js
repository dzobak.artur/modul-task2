const TriangleService = require('../services/grades.service');

async function calculateExpression(req, res) {
    const { n } = req.body;
    let sum1 = 0;
    let sum2 = 0;
  
    for (let i = 1; i <= n; i++) {
        sum1 += Math.pow(2 * i, 2);
    }

    
    for (let i = 1; i <= n; i++) {
        sum2 += Math.pow((2 * i) + 1, 3);
    }

    const result = sum1 + sum2;

    try {
        await TriangleService.saveRequest(req.body, result);
        res.json({ result });
    } catch (error) {
        console.error('Помилка в зберіганні', error);
        res.status(500).json({ error: 'Помилка серверу' });
    }
}

module.exports = {
    calculateExpression,
};
