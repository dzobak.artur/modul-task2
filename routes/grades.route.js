const express = require('express');
const router = express.Router();
const controller = require('../controllers/grades.controller');
const middleware = require('../middlewares/grades.middleware');

router.post('/calculateExpression', middleware.validateInput, controller.calculateExpression);

module.exports = router;
