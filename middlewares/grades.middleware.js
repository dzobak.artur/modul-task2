function validateInput(req, res, next) {
    const { n } = req.body;
    
    if (isNaN(n) || n <= 0 || !Number.isInteger(n)) {
      return res.status(400).json({ error: 'Число має бути не відємним ' });
    }
  
    next();
  }
  
  module.exports = {
    validateInput,
  };